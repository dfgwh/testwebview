using UnityEngine;

public class WebViewSizeController : MonoBehaviour
{
    [SerializeField] private WebViewObject _webViewObject;
    [SerializeField] private int _topMarge;

    public void Resize()
    {
        _webViewObject.SetMargins(0, _topMarge, 0, 0);
    }
}
